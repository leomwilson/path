#[macro_use]
extern crate clap;
use clap::App;

fn main() {
    let matches = App::from_yaml(load_yaml!("cli.yml")).get_matches();
    if let Some(path) = matches.value_of("PATH") {
        // handle the path
    } else if matches.is_present("build") {
        // rebuild
    } else {
        // TODO: mark discrepancies between $PATH and path
        // print the PATH
    }
}
